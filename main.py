from flask import Flask, render_template, request, url_for, redirect
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = '31.220.62.207'
app.config['MYSQL_USER'] = 'travling'
app.config['MYSQL_PASSWORD'] = 'password'
app.config['MYSQL_DB'] = 'db_review'
mysql = MySQL(app)

@app.route('/')
def home():
	male = 0
	female = 0
	avg = 0
	max = 0
	cur = mysql.connection.cursor()
	number_of_rows = cur.execute("select * from buku_tamu")
	datas = cur.fetchall()
	for data in datas:
		if data[2] == 'Laki-laki':
			male += 1
		else:
			female +=1

		if data[3] > max:
			max = data[3]

		avg += data[3]
	if number_of_rows > 0:
		avg /= number_of_rows
	cur.execute("select min(usia) from buku_tamu")
	data_mins = cur.fetchall()
	for data_min in data_mins:
		min = data_min[0]
	print(min)
	cur.close()
	return render_template('home.html', bukutamu=datas, jumlah=number_of_rows, lakilaki=male, perempuan=female, ratarata=round(avg), young=min, old=max)

@app.route('/simpan',methods=["POST"])
def simpan():
	nama = request.form['nama']
	jenis_kelamin = request.form['jenis_kelamin']
	usia = request.form['usia']
	alamat = request.form['alamat']
	cur = mysql.connection.cursor()
	cur.execute("insert into buku_tamu (nama,jenis_kelamin,usia,alamat) values (%s,%s,%s,%s)",(nama,jenis_kelamin,usia,alamat))
	mysql.connection.commit()
	return redirect(url_for('home'))

@app.route('/update', methods=["POST"])
def update():
	id_data = request.form['id']
	nama = request.form['nama']
	jenis_kelamin = request.form['jenis_kelamin']
	usia = request.form['usia']
	alamat = request.form['alamat']
	cur = mysql.connection.cursor()
	cur.execute("update buku_tamu set nama = %s , jenis_kelamin = %s , usia = %s , alamat = %s where id = %s",(nama,jenis_kelamin,usia,alamat,id_data))
	mysql.connection.commit()
	return redirect(url_for('home'))

@app.route('/hapus/<string:id_data>', methods=["GET"])
def hapus(id_data):
	cur = mysql.connection.cursor()
	cur.execute("delete from buku_tamu where id = %s",[id_data])
	mysql.connection.commit()
	return redirect(url_for('home'))

if __name__ == '__main__':
	app.run(debug=True)